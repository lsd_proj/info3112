import firebase from "firebase";
import "firebase/firestore";

// Initialize Firebase
var config = {
  apiKey: "AIzaSyCdSt8xbz37Oq0fTGysfQyoEUtQ1ZaNwg8",
  authDomain: "sprint-retrospective-e9332.firebaseapp.com",
  databaseURL: "https://sprint-retrospective-e9332.firebaseio.com",
  projectId: "sprint-retrospective-e9332",
  storageBucket: "sprint-retrospective-e9332.appspot.com",
  messagingSenderId: "193257798137"
};
//firebase.initializeApp(config);

firebase.initializeApp(config);

const dbRef = firebase.database();
export const db = firebase.firestore();
export const stories = db.collection("Stories");
export const users = db.collection("Users");
export const projects = db.collection("Projects");
export const authProvider = new firebase.auth.GoogleAuthProvider();
export const storyRef = dbRef.ref("Stories");
export const fireBase = firebase.database();
